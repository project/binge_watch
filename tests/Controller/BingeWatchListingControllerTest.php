<?php

namespace Drupal\binge_watch\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\binge_watch\ShotListServiceInterface;

/**
 * Provides automated tests for the binge_watch module.
 */
class BingeWatchListingControllerTest extends WebTestBase {

  /**
   * Drupal\binge_watch\ShotListServiceInterface definition.
   *
   * @var \Drupal\binge_watch\ShotListServiceInterface
   */
  protected $bingeWatchShotListService;


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "binge_watch BingeWatchListingController's controller functionality",
      'description' => 'Test Unit for module binge_watch and controller BingeWatchListingController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests binge_watch functionality.
   */
  public function testBingeWatchListingController() {
    // Check that the basic functions of module binge_watch.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
