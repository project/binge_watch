<?php

/**
 * @file
 * Contains shot.page.inc.
 *
 * Page callback for Shot entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Shot templates.
 *
 * Default template: shot.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_shot(array &$variables) {
  // Fetch Shot Entity Object.
  $shot = $variables['elements']['#shot'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
