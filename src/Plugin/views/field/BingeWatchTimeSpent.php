<?php

namespace Drupal\binge_watch\Plugin\views\field;

use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * Field handler to display time spent of a shot.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("binge_watch_time_spent")
 */
class BingeWatchTimeSpent extends FieldPluginBase {

  public function query() {}

  public function render(ResultRow $values) {
    return $values->_entity->getTimeSpent();
  }

}
