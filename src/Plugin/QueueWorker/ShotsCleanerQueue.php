<?php

namespace Drupal\binge_watch\Plugin\QueueWorker;

use Drupal\binge_watch\Entity\Shot;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Plugin implementation of the shots_cleaner_queue queueworker.
 *
 * @QueueWorker (
 *   id = "shots_cleaner_queue",
 *   title = @Translation("Shots cleaner queue"),
 *   cron = {"time" = 30}
 * )
 */
class ShotsCleanerQueue extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    Shot::deleteShotById([$data]);
  }

}
