<?php

namespace Drupal\binge_watch\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ShotTypeForm.
 */
class ShotTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $shot_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $shot_type->label(),
      '#description' => $this->t("Label for the Shot type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $shot_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\binge_watch\Entity\ShotType::load',
      ],
      '#disabled' => !$shot_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $shot_type = $this->entity;
    $status = $shot_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Shot type.', [
          '%label' => $shot_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Shot type.', [
          '%label' => $shot_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($shot_type->toUrl('collection'));
  }

}
