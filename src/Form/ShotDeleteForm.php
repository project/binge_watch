<?php

namespace Drupal\binge_watch\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Shot entities.
 *
 * @ingroup binge_watch
 */
class ShotDeleteForm extends ContentEntityDeleteForm {


}
