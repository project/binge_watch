<?php

namespace Drupal\binge_watch\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Shot type entities.
 */
interface ShotTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
