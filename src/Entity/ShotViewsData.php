<?php

namespace Drupal\binge_watch\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Shot entities.
 */
class ShotViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['shot']['binge_watch_time_spent'] = [
      'title' => t('Time spent'),
      'help' => t('Current time spent of a Shot entity.'),
      'field' => [
        'title' => t('Time spent'),
        'help' => t('Current time spent of a Shot entity.'),
        'id' => 'binge_watch_time_spent',
        'click sortable' => FALSE,
      ],
    ];

    return $data;
  }

}
