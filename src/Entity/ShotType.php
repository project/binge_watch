<?php

namespace Drupal\binge_watch\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Shot type entity.
 *
 * @ConfigEntityType(
 *   id = "shot_type",
 *   label = @Translation("Shot type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\binge_watch\ShotTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\binge_watch\Form\ShotTypeForm",
 *       "edit" = "Drupal\binge_watch\Form\ShotTypeForm",
 *       "delete" = "Drupal\binge_watch\Form\ShotTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\binge_watch\ShotTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "shot_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "shot",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/shot_type/{shot_type}",
 *     "add-form" = "/admin/structure/shot_type/add",
 *     "edit-form" = "/admin/structure/shot_type/{shot_type}/edit",
 *     "delete-form" = "/admin/structure/shot_type/{shot_type}/delete",
 *     "collection" = "/admin/structure/shot_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   }
 * )
 */
class ShotType extends ConfigEntityBundleBase implements ShotTypeInterface {

  /**
   * The Shot type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Shot type label.
   *
   * @var string
   */
  protected $label;

}
