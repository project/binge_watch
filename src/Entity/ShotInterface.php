<?php

namespace Drupal\binge_watch\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Shot entities.
 *
 * @ingroup binge_watch
 */
interface ShotInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Shot name.
   *
   * @return string
   *   Name of the Shot.
   */
  public function getName();

  /**
   * Sets the Shot name.
   *
   * @param string $name
   *   The Shot name.
   *
   * @return \Drupal\binge_watch\Entity\ShotInterface
   *   The called Shot entity.
   */
  public function setName($name);

  /**
   * Gets the Shot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Shot.
   */
  public function getCreatedTime();

  /**
   * Sets the Shot creation timestamp.
   *
   * @param int $timestamp
   *   The Shot creation timestamp.
   *
   * @return \Drupal\binge_watch\Entity\ShotInterface
   *   The called Shot entity.
   */
  public function setCreatedTime($timestamp);

}
