<?php

namespace Drupal\binge_watch\Entity;

use Drupal\binge_watch\Event\ShotEvent;
use Drupal\binge_watch\Event\ShotEvents;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Shot entity.
 *
 * @ingroup binge_watch
 *
 * @ContentEntityType(
 *   id = "shot",
 *   label = @Translation("Shot"),
 *   bundle_label = @Translation("Shot type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\binge_watch\ShotListBuilder",
 *     "views_data" = "Drupal\binge_watch\Entity\ShotViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\binge_watch\Form\ShotForm",
 *       "add" = "Drupal\binge_watch\Form\ShotForm",
 *       "edit" = "Drupal\binge_watch\Form\ShotForm",
 *       "delete" = "Drupal\binge_watch\Form\ShotDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\binge_watch\ShotHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\binge_watch\ShotAccessControlHandler",
 *   },
 *   base_table = "shot",
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer shot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/shot/{shot}",
 *     "add-page" = "/admin/structure/shot/add",
 *     "add-form" = "/admin/structure/shot/add/{shot_type}",
 *     "edit-form" = "/admin/structure/shot/{shot}/edit",
 *     "delete-form" = "/admin/structure/shot/{shot}/delete",
 *     "collection" = "/admin/structure/shot",
 *   },
 *   bundle_entity_type = "shot_type",
 *   field_ui_base_route = "entity.shot_type.edit_form"
 * )
 */
class Shot extends ContentEntityBase implements ShotInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  const EPISODE_FIRST_ID_DEFAULT = 0;

  const EVENT_TYPE_CREATE = 'create';
  const EVENT_TYPE_UPDATE = 'update';

  protected $timeCalc;
  protected $imdbApi;

  protected $tvShow;

  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  public static function deleteShotById($ids, &$context = NULL) {
    $shots = Shot::loadMultiple($ids);

    foreach ($shots as $shot) {
      $shot->delete();
    }
  }

  public static function loadByUid($uid) {
    $query = \Drupal::service('binge_watch.shot_list_service')->listShotsByUid($uid);
    $shot_ids = $query->execute()->fetchCol();

    return self::loadMultiple($shot_ids);
  }

  public static function loadChunksByUid($uid, $limit) {
    $shot_ids = [];

    $list_service = \Drupal::service('binge_watch.shot_list_service');

    $start = 0;
    do {
      $query = $list_service->listShotsByUid($uid, $start, $limit);
      $start += $limit;
    }
    while ($shot_ids[] = $query->execute()->fetchCol());

    // Deleting the last empty element.
    $last_key = array_key_last($shot_ids);
    if (!$shot_ids[$last_key]) {
      unset($shot_ids[$last_key]);
    }

    return $shot_ids;
  }

  public function getTimeSpent() {
    switch ($this->bundle()) {
      case 'tv_show':
        return $this->get('field_time_spent')->getValue()[0]['value'] ?? 0;
      case 'movie':
        return $this->getMovieTimeSpent();
      case 'movie_bundle':
        return $this->getMovieBundleTimeSpent();
    }
  }

  private function getMovieTimeSpent($delta = 0, $field_name = 'field_movie_shot_single') {
    $paragraph = $this->get($field_name)->referencedEntities()[$delta];

    return $paragraph->get('field_time_spent')->getValue()[0]['value'] ?? 0;
  }

  private function getMovieBundleTimeSpent($field_name = 'field_movie_shot_multiple') {
    $time = 0;

    $field_movie_shot_multiple = $this->get($field_name)->getValue();
    foreach ($field_movie_shot_multiple as $delta => $value) {
      $time += $this->getMovieTimeSpent($delta, $field_name);
    }

    return $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Shot entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Shot entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Shot is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    $tags = ['binge_watch:shot:update'];
    if ($this->isNew()) {
      $tags = ['binge_watch:shot:create'];
    }

    return Cache::mergeTags(parent::getCacheTagsToInvalidate(), $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->timeCalc = \Drupal::service('binge_watch.time_calculator');
    $this->imdbApi = \Drupal::service('imdb_api.imdb_api');
    $this->currentUser = \Drupal::currentUser();

    switch ($this->bundle()) {
      case 'tv_show':
        $this->preSaveTvShow();
        break;
      case 'movie':
        $this->preSaveMovie();
        break;
      case 'movie_bundle':
        $this->preSaveMovieBundle();
        break;
    }

    // Setting up user.
    if (!$this->get('field_user')->getValue()) {
      $this->set('field_user', $this->currentUser->id());
    }

    $this->dispatchEvents();

    parent::preSave($storage);
  }

  private function dispatchEvents($stage = 'preSave') {
    $eventDispatcher = \Drupal::service('event_dispatcher');

    $event_type = $this->isNew() ? self::EVENT_TYPE_CREATE : self::EVENT_TYPE_UPDATE;
    $event_name = constant('\Drupal\binge_watch\Event\ShotEvents::' . 'BINGE_WATCH_SHOT_' . strtoupper($stage) . '_' . strtoupper($event_type));

    $event = new ShotEvent($event_type, $stage);
    $eventDispatcher->dispatch($event, $event_name);
  }

  private function preSaveTvShow() {
    // Retrieving an episode data to be able to create TV Show entity.
    $field_tv_show_episode = $this->get('field_tv_show_episode')->getValue();
    $episode = Json::decode($field_tv_show_episode[0]['value']);

    // Setting up entity name.
    $title_prefix = $this->id() ? 'Shot #' . $this->id() . ': ' : '';
    $this->setName($title_prefix . $episode['tv_show'] . ': ' . $episode['title'] . ' (s' . $episode['season'] . 'e' . $episode['episode'] . ') - ' . $this->currentUser->getDisplayName());

    // Retrieving the TV Show.
    $this->tvShow = $this->imdbApi->createImdbEntity($episode['tv_show_pure_id'], 'tv_show');

    // If there is a TV Show count time spent.
    if ($this->tvShow) {

      $episode_original = [];
      if ($this->original) {
        $original_field_tv_show_episode = $this->original->get('field_tv_show_episode')->getValue();
        $episode_original = Json::decode($original_field_tv_show_episode[0]['value']);

        // No option to change TV show once Shot is created.
        $episode['tv_show'] = $episode_original['tv_show'];
        $episode['tv_show_pure_id'] = $episode_original['tv_show_pure_id'];
        $this->get('field_tv_show_episode')->setValue([['value' => Json::encode($episode)]]);
      }

      // Preventing the user from going back to previous season/episode.
      if (!$this->episodeChangeIsValid($episode, $episode_original)) {
        \Drupal::messenger()->addWarning(t('TV show data hasn\'t been updated due to invalid choice'));

        $this->set('field_tv_show_episode', $original_field_tv_show_episode);
      }

      if ($this->episodeIsChanged($episode, $episode_original)) {
        $this->updateTvShowTimeSpent($episode_original);
      }
    }
  }

  private function preSaveMovie($delta = 0, $field_name = 'field_movie_shot_single') {
    $field_movie_shot_single = $this->get($field_name)->getValue();
    $movie_name = $field_movie_shot_single[$delta]['subform']['field_movie'][0]['value'] ?? '';

    // If no movie has been found - do nothing.
    // If no movie has been typed in - leave to the validation.
    if (!$movie_name || !preg_match('/(?>tt)\d+/', $movie_name, $matches)) {
      return;
    }

    // Setting up entity name.
    $title_prefix = $this->id() ? 'Shot #' . $this->id() . ': ' : '';
    $this->setName($title_prefix . $movie_name . ' - ' . $this->currentUser->getDisplayName());

    $paragraph = $field_movie_shot_single[$delta]['entity'] ?? NULL;
    if (!$paragraph) {
      $paragraph = $this->get($field_name)->referencedEntities()[$delta];
    }

    $field_time_spent = $paragraph->get('field_time_spent')->getValue();
    $time = &$field_time_spent[0]['value'];

    /** @var \Imdb\Entities\Movie $movie */
    if ($movie = $this->imdbApi->createImdbEntity($matches[0], 'movie')) {
      $time = $movie->getRunningTimeInMinutes();

      // Setting time spent.
      $paragraph->set('field_time_spent', $field_time_spent);

      $this->addCacheTags([
        'binge_watch:shot:time_updated',
        'binge_watch:shot:movie:time_updated',
      ]);
    }
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    $this->dispatchEvents('postSave');

    if (!$update) {
      // Ensuring uniqueness of entity name.
      $this->setName('Shot #' . $this->id() . ': ' . $this->getName());
      $this->save();
    }
  }

  private function preSaveMovieBundle($field_name = 'field_movie_shot_multiple') {
    $field_movie_shot_multiple = $this->get($field_name)->getValue();

    // Setting up entity name.
    // @todo this doesn't work for some reason.
    if (!$this->getName()) {
      $this->setName('Movie bundle: ' . $this->currentUser->getDisplayName());
    }

    foreach ($field_movie_shot_multiple as $delta => $value) {
      $this->preSaveMovie($delta, $field_name);
    }
  }

  private function updateTvShowTimeSpent($original = []) {
    $field_time_spent = $this->get('field_time_spent')->getValue();
    $time = &$field_time_spent[0]['value'];

    // New values.
    $episode = Json::decode($this->get('field_tv_show_episode')->getValue()[0]['value']);

    // Retrieving a season and episode to start counting from
    // depending if we creating new Shot or updating an existing one.
    if ($original) {
      list('season' => $season_id, 'episode' => $episode_id) = $original;

      // Skipping current episode since it was added to time spent on a previous save.
      $episode_id++;

      // If we switching to the next season(s) calculate the remaining episodes
      // of the current season and move on.
      if ($episode['season'] != $season_id) {
        $last_episode_id = $this->imdbApi->getTvShowSeasonLastEpisodeId($this->tvShow, $season_id);
        $time += $this->timeCalc->calculateTvShowSeason($this->tvShow, $season_id, $episode_id, $last_episode_id);

        // Reseting an episode to default since we have switched to new season.
        $episode_id = self::EPISODE_FIRST_ID_DEFAULT;
      }

      // Incrementing to skip the current season which will be (was) calculated separately.
      $season_id++;
    }
    else {
      // Retrieving first season number (assuming it can be 0 or 1).
      $season_id = $this->imdbApi->getTvShowFirstSeasonId($this->tvShow);
      $episode_id = self::EPISODE_FIRST_ID_DEFAULT;
    }

    // Checking if there is (are) an entire season(s) to count time.
    if (($episode['season'] - $season_id) > 0) {
      // Retrieving an array of seasons we need to count as a whole
      // minus the last chosen season which we will count as per-episode.
      $seasons_to_calc = $this->timeCalc->getSeasonsToCalculate($this->tvShow, $season_id, ($episode['season'] - 1));

      if ($seasons_to_calc) {
        $time += $this->timeCalc->calculateSeasons($this->tvShow, array_key_first($seasons_to_calc), array_key_last($seasons_to_calc));
      }
    }

    // Calculating the time of the last chosen season.
    $time += $this->timeCalc->calculateTvShowSeason($this->tvShow, $episode['season'], $episode_id, $episode['episode']);

    // Setting new time spent.
    $this->set('field_time_spent', $field_time_spent);

    $this->addCacheTags([
      'binge_watch:shot:time_updated',
      'binge_watch:shot:tv_show:time_updated',
    ]);
  }

  private function episodeIsChanged($episode, $episode_original) {
    return Json::encode($episode) != Json::encode($episode_original);
  }

  private function episodeChangeIsValid($episode, $episode_original) {
    if (!$episode_original) {
      return TRUE;
    }

    if ($episode['season'] < $episode_original['season']) {
      return FALSE;
    }

    if (
      $episode['season'] == $episode_original['season'] &&
      $episode['episode'] < $episode_original['episode']
    ) {
      return FALSE;
    }

    return TRUE;
  }

  public static function getUserTvShows($uid = NULL, $table = 'shot__field_tv_show_episode') {
    $uid = $uid ? $uid : \Drupal::currentUser()->id();

    $db = \Drupal::database();
    $query = $db->select('shot__field_user', 'sfu');
    $query->innerJoin($table, 'sftse', 'sfu.entity_id=sftse.entity_id');
    $query->fields('sftse', ['field_tv_show_episode_value']);
    $query->condition('sfu.field_user_target_id', $uid);

    return $query->execute()->fetchCol();
  }

  public static function getUserMovies($options) {
    $uid = $options['uid'] ?? \Drupal::currentUser()->id();

    $db = \Drupal::database();
    $query = $db->select('shot__field_user', 'sfu');
    $query->innerJoin($options['relation_table'], 'sfms', 'sfu.entity_id=sfms.entity_id');
    $query->innerJoin($options['table'], 'pfm', 'sfms.' . $options['relation_field'] . '=pfm.entity_id');
    $query->fields('pfm', ['field_movie_value']);
    $query->condition('sfu.field_user_target_id', $uid);

    return $query->execute()->fetchAllKeyed(0, 0);
  }

  public static function generateTvShowIdsRegExp($episodes) {
    $tv_show_ids = [];
    foreach ($episodes as $episode) {
      $tv_show_id = Json::decode($episode)['tv_show_pure_id'];
      $tv_show_ids[$tv_show_id] = $tv_show_id;
    }

    return '"tv_show_pure_id":"(?>' . implode('|', $tv_show_ids) . ')"';
  }

}
