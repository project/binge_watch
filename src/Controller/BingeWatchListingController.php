<?php

namespace Drupal\binge_watch\Controller;

use Drupal\binge_watch\Entity\Shot;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BingeWatchListingController.
 */
class BingeWatchListingController extends ControllerBase {

  /**
   * Drupal\binge_watch\ShotListServiceInterface definition.
   *
   * @var \Drupal\binge_watch\ShotListServiceInterface
   */
  protected $shotListService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // @todo fix lists when there is no paragraphs enabled.
    $instance = parent::create($container);
    $instance->shotListService = $container->get('binge_watch.shot_list_service');
    return $instance;
  }

  /**
   * List users.
   *
   * @return array
   *   Return a list of users.
   */
  public function listUsers() {
    $build = [];

    $header = [
      ['data' => $this->t('User ID'), 'field' => 'u.uid'],
      ['data' => $this->t('Time spent'), 'field' => 'time_value'],
    ];

    $result = $this->shotListService->listUsersByTime()
      ->orderByHeader($header)
      ->execute();

    $rows = [];
    foreach ($result as $row) {
      $rows[] = ['data' => (array) $row];
    }

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    $build['pager'] = ['#type' => 'pager'];

    // Providing cache metadata.
    $build['table']['#cache'] = $build['pager']['#cache'] = [
      'max-age' => 60,
      'tags' => [
        'binge_watch:shot:time_updated',
      ],
    ];

    return $build;
  }
  /**
   * List shots.
   *
   * @return array
   *   Return a list of shots.
   */
  public function listShots() {
    $build = [];

    $header = [
      ['data' => $this->t('ID'), 'field' => 's.id'],
      ['data' => $this->t('Type'), 'field' => 's.type'],
      ['data' => $this->t('Name'), 'field' => 's.name'],
      ['data' => $this->t('User ID'), 'field' => 'sfu.field_user_target_id'],
      ['data' => $this->t('Time spent'), 'field' => 'time_value'],
    ];

    $result = $this->shotListService->listShotsByTime()
      ->orderByHeader($header)
      ->execute();

    $rows = [];
    foreach ($result as $row) {
      $rows[] = ['data' => (array) $row];
    }

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    $build['pager'] = ['#type' => 'pager'];

    // Providing cache metadata.
    $build['table']['#cache'] = $build['pager']['#cache'] = [
      'max-age' => 60,
      'tags' => [
        'binge_watch:shot:create',
        'binge_watch:shot:update',
      ],
    ];

    return $build;
  }

  public function listRelatedUsers(AccountInterface $user, Request $request) {
    $build = [];

    $header = [
      ['data' => $this->t('User ID'), 'field' => 'sfu.field_user_target_id'],
      ['data' => $this->t('Relevancy'), 'field' => 'occurrences'],
    ];

    $rows = [];
    if ($query = $this->shotListService->listRelatedUsers($user->id())) {
      $result = $query
        ->orderByHeader($header)
        ->execute();

      foreach ($result as $row) {
        $rows[] = ['data' => (array) $row];
      }
    }

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    $build['pager'] = ['#type' => 'pager'];

    // Providing cache metadata.
    $build['table']['#cache'] = $build['pager']['#cache'] = [
      'max-age' => 60,
      'tags' => [
        'binge_watch:shot:create',
      ],
    ];

    return $build;
  }

}
