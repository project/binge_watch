<?php

namespace Drupal\binge_watch;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\binge_watch\Entity\Shot;

/**
 * Provides dynamic permissions for Shot of different types.
 *
 * @ingroup binge_watch
 *
 */
class ShotPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of shot type permissions.
   *
   * @return array
   *   The Shot by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];

    foreach (Shot::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of shot permissions for a given shot type.
   *
   * @param \Drupal\binge_watch\Entity\Shot $type
   *   The Shot type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(Shot $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "$type_id create entities" => [
        'title' => $this->t('Create new %type_name entities', $type_params),
      ],
      "$type_id edit own entities" => [
        'title' => $this->t('Edit own %type_name entities', $type_params),
      ],
      "$type_id edit any entities" => [
        'title' => $this->t('Edit any %type_name entities', $type_params),
      ],
      "$type_id delete own entities" => [
        'title' => $this->t('Delete own %type_name entities', $type_params),
      ],
      "$type_id delete any entities" => [
        'title' => $this->t('Delete any %type_name entities', $type_params),
      ],
    ];
  }

}
