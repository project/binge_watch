<?php

namespace Drupal\binge_watch;

use Drupal\binge_watch\Entity\Shot;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

class ShotListService implements ShotListServiceInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new DatabaseLockBackend.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(Connection $database, ModuleHandlerInterface $module_handler) {
    $this->database = $database;
    $this->moduleHandler = $module_handler;
  }

  /*public function listUsersByTime($order = 'DESC', $bundles = []) {
    // @todo make it dynamic queries. And add pager.
    $query_options = [];
    $query_string = '
SELECT
       (Coalesce(SUM({shot__field_time_spent}.field_time_spent_value), 0) + Coalesce(SUM({paragraph__field_time_spent}.field_time_spent_value), 0)) time_value,
       uid
FROM {users}
LEFT JOIN {shot__field_user} ON {users}.uid={shot__field_user}.field_user_target_id
LEFT JOIN {shot__field_time_spent} ON {shot__field_user}.entity_id={shot__field_time_spent}.entity_id
LEFT JOIN {paragraphs_item_field_data} ON {shot__field_user}.entity_id={paragraphs_item_field_data}.parent_id
LEFT JOIN {paragraph__field_time_spent} ON {paragraphs_item_field_data}.id={paragraph__field_time_spent}.entity_id';

    if ($bundles) {
      $query_string .= '
      LEFT JOIN {shot} ON {shot}.id={shot__field_user}.entity_id
      WHERE {shot}.type IN (:bundles[])';
      $query_options[':bundles[]'] = $bundles;
    }

    $query_string .= '
    WHERE {users}.uid <> 0
    GROUP BY {users}.uid
    ORDER BY time_value ' . $order;

    $query = $this->database->query($query_string, $query_options);
    return $query->fetchAll();
  }*/

  public function listUsersByTime($order = 'DESC', $bundles = []) {
    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query = $this->database->select('users', 'u')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender');

    $query->addExpression('(Coalesce(SUM(sfts.field_time_spent_value), 0) + Coalesce(SUM(pfts.field_time_spent_value), 0))', 'time_value');
    $query->fields('u', ['uid']);
    $query->leftJoin('shot__field_user', 'sfu', 'u.uid=sfu.field_user_target_id');
    $query->leftJoin('shot__field_time_spent', 'sfts', 'sfu.entity_id=sfts.entity_id');
    $query->leftJoin('paragraphs_item_field_data', 'pifd', 'sfu.entity_id=pifd.parent_id');
    $query->leftJoin('paragraph__field_time_spent', 'pfts', 'pifd.id=pfts.entity_id');

    if ($bundles) {
      $query->leftJoin('shot', 's', 's.id=sfu.entity_id');
      $query->condition('s.type', $bundles);
    }

    $query->condition('u.uid', 0, '<>');

    $query->limit();

    $query->groupBy('u.uid');

    $query->orderBy('time_value', $order);

    return $query;
  }

  /*public function listShotsByTime($order = 'DESC', $bundles = [], $account = NULL) {
    $query_options = [];
    $query_string = '
SELECT
       (Coalesce(SUM({shot__field_time_spent}.field_time_spent_value), 0) + Coalesce(SUM({paragraph__field_time_spent}.field_time_spent_value), 0)) time_value,
       {shot}.id,
       {shot}.type,
       {shot}.name
FROM {shot}
LEFT JOIN {shot__field_time_spent} ON {shot}.id={shot__field_time_spent}.entity_id
LEFT JOIN {paragraphs_item_field_data} ON {shot}.id={paragraphs_item_field_data}.parent_id
LEFT JOIN {paragraph__field_time_spent} ON {paragraphs_item_field_data}.id={paragraph__field_time_spent}.entity_id';

    if ($account) {
      $query_string .= '
      LEFT JOIN {shot__field_user} ON {shot}.id={shot__field_user}.entity_id
      WHERE {shot__field_user}.field_user_target_id=:uid';
      $query_options[':uid'] = $account->id();
    }

    if ($bundles) {
      $query_string .= ' WHERE {shot}.type IN (:bundles[])';
      $query_options[':bundles[]'] = $bundles;
    }

    $query_string .= '
    GROUP BY {shot}.id, {shot}.type, {shot}.name
    ORDER BY time_value ' . $order;

    $query = $this->database->query($query_string, $query_options);
    return $query->fetchAll();
  }*/

  public function listShotsByTime($order = 'DESC', $bundles = [], $account = NULL) {
    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query = $this->database->select('shot', 's')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender');

    $query->addExpression('(Coalesce(SUM(sfts.field_time_spent_value), 0) + Coalesce(SUM(pfts.field_time_spent_value), 0))', 'time_value');
    $query->fields('s', ['id', 'type', 'name']);
    $query->addField('sfu', 'field_user_target_id');
    $query->leftJoin('shot__field_time_spent', 'sfts', 's.id=sfts.entity_id');
    $query->leftJoin('paragraphs_item_field_data', 'pifd', 's.id=pifd.parent_id');
    $query->leftJoin('paragraph__field_time_spent', 'pfts', 'pifd.id=pfts.entity_id');
    $query->leftJoin('shot__field_user', 'sfu', 's.id=sfu.entity_id');

    if ($account) {
      $query->condition('sfu.field_user_target_id', $account->id());
    }

    if ($bundles) {
      $query->condition('s.type', $bundles);
    }

    $this->moduleHandler->alter('shots_list_query', $query);

    $query->limit();

    $query->groupBy('s.id');
    $query->groupBy('s.type');
    $query->groupBy('s.name');
    $query->groupBy('sfu.field_user_target_id');

    $query->orderBy('time_value', $order);

    return $query;
  }

  public function listShotsByUid($uid, $start = NULL, $length = NULL) {
    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query = $this->database->select('shot__field_user', 'sfu');

    $query->fields('sfu', ['entity_id']);
    $query->condition('sfu.field_user_target_id', $uid);
    $query->orderBy('sfu.entity_id');

    if (isset($start) && $length) {
      $query->range($start, $length);
    }

    return $query;
  }

  public function listRelatedUsers($uid, $order = 'DESC', $start = NULL, $length = NULL) {
    // Gathering all Movies into array.
    $movies = [];
    $relation_tables = [
      'shot__field_movie_shot_single',
      'shot__field_movie_shot_multiple',
    ];
    foreach ($relation_tables as $relation_table) {
      preg_match('/^shot__field_movie_shot_(single|multiple)$/', $relation_table, $matches);

      $movies += Shot::getUserMovies([
        'relation_table' => $relation_table,
        'relation_field' => 'field_movie_shot_' . $matches[1] . '_target_id',
        'table' => 'paragraph__field_movie',
        'uid' => $uid,
      ]);
    }

    // Setting up query.
    $query = $this->database->select('shot__field_user', 'sfu')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender');

    $query->addExpression('COUNT(*)', 'occurrences');
    $query->fields('sfu', ['field_user_target_id']);
    $query->leftJoin('shot__field_tv_show_episode', 'sftse', 'sfu.entity_id=sftse.entity_id');
    $query->leftJoin('paragraphs_item_field_data', 'pifd', 'sfu.entity_id=pifd.parent_id');
    $query->leftJoin('paragraph__field_movie', 'pfm', 'pifd.id=pfm.entity_id');

    $query->condition('sfu.field_user_target_id', $uid, '<>');

    $query_or = $query->orConditionGroup();
    // Getting episodes for the chosen user.
    if ($episodes = Shot::getUserTvShows()) {
      // Gathering all TV Show IDs into regular expression.
      $regexp = Shot::generateTvShowIdsRegExp($episodes);
      $query_or->condition('sftse.field_tv_show_episode_value', $regexp, 'REGEXP');
    }
    // Setting up condition for Movies if exist.
    if ($movies) {
      $query_or->condition('pfm.field_movie_value', $movies, 'IN');
    }
    $query->condition($query_or);

    // If there aren't matches in neither TV shows nor movies.
    if (!$episodes && !$movies) {
      return FALSE;
    }

    $query->groupBy('sfu.field_user_target_id');
    $query->orderBy('occurrences', $order);

    if (isset($start) && $length) {
      $query->range($start, $length);
    }

    return $query;
  }

}
