<?php

namespace Drupal\binge_watch;

use Drupal\Core\Session\AccountInterface;
use Imdb\Entities\TvShow;

/**
 * Interface CalculatorInterface.
 */
interface TimeCalculatorInterface {

  public function calculateSeasons(TvShow $tv_show, int $from = NULL, int $till = NULL);
  public function calculateTvShowSeason(TvShow $tv_show, int $season, int $from = NULL, int $till = NULL);
  public function calculateSeason($season, int $from = NULL, int $till = NULL);

  public function calculateTvShows($tv_shows = []);

  public function calculateMovies($movies = []);

  public function calculateTotal(AccountInterface $account = NULL, $shot_type = NULL);

  public function getSeasonsToCalculate(TvShow $tv_show, int $from = NULL, int $till = NULL);

}
