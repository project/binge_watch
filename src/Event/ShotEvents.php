<?php

namespace Drupal\binge_watch\Event;

final class ShotEvents {

  const BINGE_WATCH_SHOT_PRESAVE_CREATE = 'binge_watch.shot.preSave.create';
  const BINGE_WATCH_SHOT_PRESAVE_UPDATE = 'binge_watch.shot.preSave.update';

  const BINGE_WATCH_SHOT_POSTSAVE_CREATE = 'binge_watch.shot.postSave.create';
  const BINGE_WATCH_SHOT_POSTSAVE_UPDATE = 'binge_watch.shot.postSave.update';

}
