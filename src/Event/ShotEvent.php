<?php

namespace Drupal\binge_watch\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class to create Shot events.
 */
class ShotEvent extends Event {

  /**
   * Type.
   *
   * @var string
   */
  protected $type;

  /**
   * Stage.
   *
   * @var string
   */
  protected $stage;

  /**
   * Constructs Shot event object.
   *
   * @param string $type
   *   Type.
   * @param string $stage
   *   Stage.
   */
  public function __construct($type, $stage) {
    $this->type = $type;
    $this->stage = $stage;
  }

  /**
   * Get type.
   *
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get stage.
   *
   * @return string
   */
  public function getReport() {
    return $this->stage;
  }

}
