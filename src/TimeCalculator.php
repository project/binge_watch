<?php

namespace Drupal\binge_watch;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\imdb_api\ImdbApiInterface;
use Drupal\imdb_api\ImdbApiService;
use Imdb\Entities\TvShow;

/**
 * Class Calculator.
 */
class TimeCalculator implements TimeCalculatorInterface {

  /**
   * The IMDB API service.
   *
   * @var \Drupal\imdb_api\ImdbApiInterface
   */
  protected $imdbApi;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a Calculator object.
   *
   * @param \Drupal\imdb_api\ImdbApiInterface $imdb_api
   *   The IMDB API service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(ImdbApiInterface $imdb_api, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->imdbApi = $imdb_api;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  public function calculateSeasons(TvShow $tv_show, int $from = NULL, int $till = NULL) {
    $time = 0;

    $seasons = ImdbApiService::getTvShowSeasons($tv_show);
    foreach ($seasons as $season) {

      if (isset($from) && $season->season < $from) {
        continue;
      }
      if (isset($till) && $season->season > $till) {
        break;
      }

      $time += $this->calculateSeason($season);
    }

    return $time;
  }

  public function calculateTvShowSeason(TvShow $tv_show, int $season, int $from = NULL, int $till = NULL) {
    $time = 0;

    foreach (ImdbApiService::getTvShowSeasons($tv_show) as $season_entity) {
      if ($season_entity->season == $season) {
        $time += $this->calculateSeason($season_entity, $from, $till);
      }
    }

    return $time;
  }

  public function calculateSeason($season, int $from = NULL, int $till = NULL) {
    $time = 0;

    foreach ($season->episodes as $episode) {

      if (isset($from) && $episode->episode < $from) {
        continue;
      }
      if (isset($till) && $episode->episode > $till) {
        break;
      }

      /** @var \Imdb\Entities\Episode $episode */
      if (!$episode = $this->loadEpisode($episode)) {
        // @todo add logging.
        continue;
      }

      $time += $episode->getRunningTimeInMinutes();
    }

    return $time;
  }

  public function calculateTvShows($tv_shows = []) {
    $time = 0;

    /** @var \Imdb\Entities\TvShow $tv_show */
    foreach ($tv_shows as $tv_show) {
      $time += $this->calculateSeasons($tv_show);
    }

    return $time;
  }

  public function calculateMovies($movies = []) {
    $time = 0;

    /** @var \Imdb\Entities\Movie $movie */
    foreach ($movies as $movie) {
      $time += $movie->getRunningTimeInMinutes();
    }

    return $time;
  }

  public function calculateTotal(AccountInterface $account = NULL, $shot_type = NULL) {
    $time = 0;

    // @todo move loading Shots by user to \Drupal\binge_watch\Entity\Shot.
    $storage = $this->entityTypeManager->getStorage('shot');

    $query = $storage->getQuery();

    if ($shot_type) {
      $query->condition('type', $shot_type);
    }

    $uid = $account ? $account->id() : $this->currentUser->id();
    $query->condition('field_user.entity.uid', $uid);

    if ($shot_ids = $query->execute()) {
      $shots = $storage->loadMultiple($shot_ids);

      // Calculating total time.
      foreach ($shots as $shot) {
        $time += $shot->getTimeSpent();
      }
    }

    return $time;
  }

  private function loadEpisode($episode) {
    preg_match('/^\/title\/(.+)\/$/', $episode->id, $matches);

    return $this->imdbApi->createImdbEntity($matches[1], 'episode');
  }

  public function getSeasonsToCalculate(TvShow $tv_show, int $from = NULL, int $till = NULL) {
    $ids = [];

    $seasons = ImdbApiService::getTvShowSeasons($tv_show);
    foreach ($seasons as $season) {

      if (isset($from) && $season->season < $from) {
        continue;
      }
      if (isset($till) && $season->season > $till) {
        break;
      }

      $ids[$season->season] = $season->season;
    }

    return $ids;
  }
}
