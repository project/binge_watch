<?php

namespace Drupal\binge_watch;

interface ShotListServiceInterface {

  public function listUsersByTime($order = 'DESC', $bundles = []);
  public function listShotsByTime($order = 'DESC', $bundles = [], $account = NULL);

}
