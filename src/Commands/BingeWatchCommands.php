<?php

namespace Drupal\binge_watch\Commands;

use Drupal\binge_watch\Entity\Shot;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\binge_watch\ShotListServiceInterface;
use Drush\Commands\DrushCommands;

class BingeWatchCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The Shot list service.
   *
   * @var \Drupal\binge_watch\ShotListServiceInterface
   */
  protected $shotListing;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory service.
   * @param \Drupal\binge_watch\ShotListServiceInterface $shot_listing
   *   The Shot list service.
   */
  public function __construct(QueueFactory $queue_factory, ShotListServiceInterface $shot_listing) {
    parent::__construct();

    $this->queueFactory = $queue_factory;
    $this->shotListing = $shot_listing;
  }

  /**
   * Delete Shots.
   *
   * @param string $uid
   *   The user ID.
   * @param string $batch_size
   *   The batch size.
   * @command binge_watch:delete-shots
   * @aliases binge_watch:ds
   * @option notify-user
   *   Send an email to user about his/her Shots has been deleted.
   * @usage binge_watch:delete-shots --notify-user
   *   Deletes Shots and notifies user about it.
   */
  public function deleteShots(string $uid = '', string $batch_size = '') {
    $shot_ids = Shot::loadChunksByUid($uid, $batch_size);

    $operations = [];
    foreach ($shot_ids as $key => $shot_ids_set) {

      $operations[] = [
        '\Drupal\binge_watch\Entity\Shot::deleteShotById',
        [$shot_ids_set],
      ];
    }

    $batch = [
      'title' => $this->t('Deleting Shots'),
      'operations' => $operations,
      'finished' => '\Drupal\binge_watch\Commands\BingeWatchCommands::deleteShotsFinished',
    ];

    batch_set($batch);
    drush_backend_batch_process();

    $this->output()->writeln('Shots are now being deleted.');
  }

  /**
   * Queue Shots for deletion.
   *
   * @param string $uid
   *   The user ID.
   * @command binge_watch:queue-shots-for-deletion
   * @aliases binge_watch:qsfd
   * @option notify-user
   *   Send an email to user about his/her Shots has been deleted.
   * @usage binge_watch:queue-shots-for-deletion --notify-user
   *   Queues Shots for deletion and notifies user about it.
   */
  public function queueShotsForDeletion(string $uid = '') {
    $query = $this->shotListing->listShotsByUid($uid);
    $shot_ids = $query->execute()->fetchCol();

    $queue = $this->queueFactory->get('shots_cleaner_queue');
    $queue->createQueue();

    foreach ($shot_ids as $shot_id) {
      $queue->createItem($shot_id);
    }
  }

  public static function deleteShotsFinished($success, array $results, array $operations) {}

}
