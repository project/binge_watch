<?php

namespace Drupal\binge_watch;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Shot entities.
 *
 * @ingroup binge_watch
 */
class ShotListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Shot ID');
    $header['name'] = $this->t('Name');
    $header['time_spent'] = $this->t('Time spent');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\binge_watch\Entity\Shot $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.shot.edit_form',
      ['shot' => $entity->id()]
    );
    $row['time_spent'] = $entity->getTimeSpent();
    return $row + parent::buildRow($entity);
  }

}
